#pragma once
#ifndef CHIP_H
#define CHIP_H
#include <SFML/Graphics.hpp>

class chip
{
private:
	char color; //red or black
	sf::Vector2f position;
	sf::CircleShape chippiece;
public:
	chip(char colorIn);
	const char getcolor();
	void setcolor(char inColor);
	sf::Vector2f getposition();
	void setposition(int xIn, int yIn);
};

#endif