#include "Tree.h"


Node::Node(char inColor)
{
	color = inColor;
	for (int i = 0; i < BoardWidth; i++)
	{
		Children[i] = NULL;
	}
}

Node::Node()
{
	for (int i = 0; i < BoardWidth; i++)
	{
		Children[i] = NULL;
	}
}

const char Node::getColor()
{
	return color;
}

const int Node::getScore()
{
	return score;
}

game * Node::getGame()
{
	return PsuedoGame;
}




Node * Node::getChild(int iterator)
{
	return Children[iterator];
}

void Node::setColor(char inColor)
{
	color = inColor;
}

void Node::setScore(int inScore)
{
	score = inScore;
}

void Node::setGame(game * inGame)
{
	PsuedoGame = inGame;
}

void Node::setChild(Node * inNode, int Index)
{
	Children[Index] = inNode;
}

void Node::setParent(Node * inNode)
{
	Parent = inNode;
}


Tree::Tree(game* inGame)
{
	Node* root = new Node('B'); //Since AI is always black, root node should be black :)
	root->setParent(NULL); //For safety, I can edit this later 
	root->setGame(inGame);
	BuildTree(1, 4, root, inGame);
	RootNode = root;
	EvaluateTree(RootNode);
}


Tree::~Tree()
{
}

void Tree::BuildTree(int height, int maxHeight, Node* parent, game* Game)
{//This builds like a DFS fashion
	Node * insertNode = new Node();
	if (height == maxHeight) return; //happy cases only :)
	for (int i = 0; i < BoardWidth; i++)
	{
		if (!Game->getboard()->isColumnFull(i))
		{
			Node * insertNode = new Node();
			game * tempGame = new game();
			insertNode->setGame(tempGame);
			tempGame->copyBoard(Game->getboard());
			char CurrColor = 'R';
			if (parent->getColor() == 'R')
				CurrColor = 'B';
			tempGame->getboard()->insert(CurrColor, i);
			insertNode->setColor(CurrColor);
			for (int j = 0; j < BoardWidth; j++)
			{
				insertNode->setChild(NULL, j);
			}
			insertNode->setParent(parent);
			insertNode->setGame(tempGame);
			parent->setChild(insertNode, i);
		}
		else
		{
			Node * insertNode = new Node();
			insertNode->setScore(-1);
			parent->setChild(insertNode, i);

		}
	} //The bug that coems up is due to the fact that the ndoe is filled. if the node is filled we should automatically set it's score to -1? And if the score finder finds -1 it ignores the node. There may be a better way ardoun
	//this issue, but this may do for now :)

	for (int i = 0; i < BoardWidth; i++)
	{
		if(parent->getChild(i)->getScore() != -1)
			BuildTree(height + 1, maxHeight, parent->getChild(i), parent->getChild(i)->getGame());
	}
}




int Tree::EvaluateTree(Node * Root)
{//We will want to visit all of the child nodes and see if they are leaves. This should be done recursivley, done from root down by analyzing the children's value.
	//It would be wise to return a value of pairs. Consult STL for more information.
	if (Root->getScore() == -1) return -1;
	bool flag = 1;
	int minValue = 10000;
	int maxValue = -10000;
	for (int i = 0; i < BoardWidth; i++)
	{
		if (Root->getChild(i) != NULL)
			flag = 0;
	}

	if (flag == 1)
	{
		int value = 0;
		for (int i = 0; i < BoardWidth; i++)
		{
			board* GameBoard = Root->getGame()->getboard();
			value += 75 * GameBoard->check3(i, BoardHeight - 1 - GameBoard->getTop(i), 'B');
			value += 50 * GameBoard->check3(i, BoardHeight - 1 - GameBoard->getTop(i), 'R');
			if (Root->getGame()->aiLose().x != -1)
				value += -10000; //This means if the ai places a chip here it sets up human player for victory;
			if (Root->getGame()->aiWin().x != -1)
				value += 10000;
			//Add nearby values thing here.
			Root->setScore(value);
			return value;
		}
	}
	else
	{
		for (int i = 0; i < BoardWidth; i++)
		{
			int tempVal = EvaluateTree(Root->getChild(i));
			if (tempVal < minValue && tempVal != -1) //Oh the glories of magic string/ magic numbers take me away!
				minValue = tempVal;
			if (tempVal > maxValue && tempVal != -1)
				maxValue = tempVal;
		}

		if (Root->getColor() == 'R')
		{
			Root->setScore(minValue);
			return minValue;
		}
		else
		{
			Root->setScore(maxValue);
			return maxValue;
		}
	}
}



Pair Tree::findBestValue()
{//Finds best value of all child Nodes. This proves once and for all that there is a favorite child.
	return{ 1,1 };
}

void Tree::print()
{
}

Node * Tree::GetRootNode()
{
	return RootNode;
}

int Tree::findVal(int inVal)
{
	int index = -1;
	for (int i = 0; i < BoardWidth; i++)
	{
		if (this->RootNode->getScore() == inVal)
			index = i;
	}
	srand(time(NULL));
	if (index = -1)
		index = rand() % BoardWidth;
	return index;
}

