#include "Board.h"
#include "Chip.h"
#include <iostream>
#include <limits>
#include <time.h>
#include "game.h"
#include "Board.h"
#include <SFML/Graphics.hpp>

using namespace std;
//To do fix last blinking chip so only 1 chip blinks. Done
//Move victory condition to each time a chip is inserted Done
//Create smarter AI to make plan on what chips 4 in a row it can make
//Create a chance for the AI to make mistakes and not put in chip correctly

/*	cout << "User is 'R', Computer is 'B', Unused Space is White." << endl;
	cout << "Numbers beneath board are position entered to insert chip" << endl;
	cout << "Victory is acheived when 4 of your color is found in any direction" << endl;
	cout << " (left, up, up->left, etc.)" << endl;*/
void main()
{

	game thisGame;

	sf::RenderWindow window(sf::VideoMode(640, 480), "Connect 4");
	sf::Vector2f rectSize(640, 240);
	sf::RectangleShape Top;
	Top.setFillColor(sf::Color::Red);
	Top.setSize(rectSize);

	sf::RectangleShape Bottom;
	Bottom.setFillColor(sf::Color::Blue);
	Bottom.setSize(rectSize);
	Bottom.setPosition(0, 240);

	sf::Text text; //Consider making this a function call
	sf::Font font;
	font.loadFromFile("arial.ttf");
	text.setFont(font);
	text.setOutlineColor(sf::Color::Black);
	text.setOutlineThickness(2);
	text.setString("Single Player Vs AI");
	text.setCharacterSize(50);
	text.setPosition(90, 70);

	sf::Text text2; //Consider making this a function call
	text2.setFont(font);
	text2.setOutlineColor(sf::Color::Black);
	text2.setOutlineThickness(2);
	text2.setString("2 Player");
	text2.setCharacterSize(50);
	text2.setPosition(220, 310);


	window.clear(sf::Color(255, 255, 255, 255));
	window.draw(Top);
	window.draw(Bottom);
	window.draw(text);
	window.draw(text2);
	window.display();
	
	sf::Event event;
	int flag = 0;
	while (flag != 1)
	{
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::MouseButtonPressed)
			{
				float MouseX = sf::Mouse::getPosition(window).x;
				float MouseY = sf::Mouse::getPosition(window).y;
				if (MouseX >= 0 && MouseX <= 640)
				{
					if (MouseY >= 0 && MouseY <= 240)
					{
						thisGame.setPvP(0);
						flag = 1;
					}
					if (MouseY >= 240 && MouseY <= 480)
					{
						thisGame.setPvP(1);
						flag = 1;
					}
				}
			}
		}
	}

	sf::Texture BackgroundBoard;
	BackgroundBoard.loadFromFile("Connect4Board.png");
	sf::Sprite Background(BackgroundBoard);

	window.clear(sf::Color(255, 255, 255, 255));
	window.draw(Background);
	window.display();



	int chipCounter = 0; //is incremented whenever a player adds a chip, even numbers are red's turn, odd numbers are block's turn
	while (window.isOpen())
	{
		while (window.pollEvent(event))
		{
			thisGame.render(window);
			if (event.type == sf::Event::Closed)
				window.close();


			if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::Left)
				{
					thisGame.updateScrollPositionDown();
					thisGame.render(window);
				}
				else if (event.key.code == sf::Keyboard::Right)
				{
					
					thisGame.updateScrollPositionUp();
					thisGame.render(window);
				}
				else if (event.key.code == sf::Keyboard::Return && thisGame.getPVP() == 1)
				{
					if (chipCounter % 2 == 0)
						thisGame.playGame(window, 'R');
					else
						thisGame.playGame(window, 'B');
					chipCounter++;
				}
				else if (event.key.code == sf::Keyboard::Return && thisGame.getPVP() == 0)
				{
					thisGame.playGame(window, 'R');
				}
				else std::cout << "Wrong Button" << std::endl;
			}
			window.clear(sf::Color(255, 255, 255, 255));
			window.draw(Background);

			thisGame.getboard()->renderBoard(window);
			thisGame.render(window);
			window.display();

		}

	}

	getchar();
	return;
}
