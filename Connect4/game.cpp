#include "game.h"
#include "Board.h"
#include "Tree.h"
#include <SFML/Graphics.hpp>
game::game()
{
	gameboard = new board();
	ScrollPosition = 0;
}

game::~game()
{
	delete gameboard;
}
sf::Vector2i game::aiLose()
{
	sf::Vector2i BlockLose(-1, -1);
	bool flag = 0;
	game test;
	test.copyBoard(gameboard);
	for (int i = 0; i < BoardWidth; i++)
	{
		test.getboard()->insert('R', i);
		if (test.getboard()->victory('R'))
		{
			BlockLose.x = i;
			break;
		}
		test.copyBoard(gameboard);
	}

	return BlockLose;
}

sf::Vector2i game::aiWin()
{
	sf::Vector2i BlockWin(-1, -1);
	bool flag = 0;
	game* test = new game();
	test->copyBoard(gameboard);
	for (int i = 0; i < BoardWidth; i++)
	{
		test->getboard()->insert('B', i);
		if (test->getboard()->victory('B'))
		{
			BlockWin.x = i;
			break;
		}
		test->copyBoard(gameboard);
	}
	return BlockWin;
}

int game::aiEvaluate()
{//This will evaluate the values of each column on the board by a value. Values are assigned based on proximity to other similiar colors (Worth 5 for each chip), If it can block the user from getting 3 colors in a row (Worth 50 for each block), and if it can create a 3 in a row set (worth 75 each)


	int values[7] = { 0 };
	game * CopiedGame = new game();
	board* GameBoard = this->getboard();
	CopiedGame->copyBoard(GameBoard);

	Tree* GameTree = new Tree(CopiedGame);
	int insertIndex = GameTree->findVal(GameTree->GetRootNode()->getScore()); //Find index of value found in rootnode.
	return insertIndex;


	/*
	for (int i = 0; i < BoardWidth; i++)
	{
		values[i] += 75 * GameBoard->check3(i, BoardHeight-1-GameBoard->getTop(i), 'B');
		values[i] += 50 * GameBoard->check3(i, BoardHeight - 1 - GameBoard->getTop(i), 'R');
		CopiedGame->getboard()->insert('B',i);
		if (CopiedGame->aiLose().x != -1)
			values[i] += -10000; //This means if the ai places a chip here it sets up human player for victory;
		if (GameBoard->isColumnFull(i))
			values[i] += -1000000; //This means that the gameboard at this spot is full
		CopiedGame->copyBoard(GameBoard);
		//Add nearby values thing here.
	}
	//Check for highest value
	int BestColumn = -1;
	int BestValue = 0;
	for (int i = 0; i < BoardWidth; i++) //Make this more random later so that way its placements are less predicable?
	{
		if (values[i] > BestColumn)
		{
			BestColumn = i;
			BestValue = values[i];
		}
	}
	srand(time(NULL)); // Create random seed
	if (BestValue == 0)
	{
		do
		{
			BestColumn = rand() % BoardWidth;
		} while (this->getboard()->isColumnFull(BestColumn) && values[BestColumn] == 0);
	}

	return BestColumn;*/
}


void game::playGame(sf::RenderWindow& l_window, char color) // Computer vs AI
{
	//IDEA:Create random number to see if computer will block lose or go through with win or block or other logic.
	srand(time(NULL));
	int randomNumber = rand() % 100;
	if (playerVsplayer == 0)
	{
		if (!gameboard->insert('R', getScrollPosition())) return;
		else
		{
			this->getboard()->renderBoard(l_window);
			this->render(l_window);
			l_window.display();
			checkvictory(l_window);
		}
		if (this->getboard()->isFull())
		{
			sf::Event event;
			WriteToScreen("Draw, Press R for New Game!", l_window);
			while ((l_window.pollEvent(event)))
			{
				this_thread::sleep_for(1000ms);
				l_window.display();
				if (event.key.code == sf::Keyboard::R)
					break;

			}
			this->getboard()->clearboard();
			return;
		}
		if (this->aiWin().x != -1 && randomNumber)
		{
			gameboard->insert('B', this->aiWin().x);
			this->getboard()->renderBoard(l_window);
			this->render(l_window);
			l_window.display();
			checkvictory(l_window);

		}
		else if (this->aiLose().x != -1 && randomNumber)
		{
			gameboard->insert('B', this->aiLose().x);
			this->getboard()->renderBoard(l_window);
			this->render(l_window);
			l_window.display();
			checkvictory(l_window);
		}
		else {
			gameboard->insert('B',this->aiEvaluate());
			this->getboard()->renderBoard(l_window);
			this->render(l_window);
			l_window.display();
			checkvictory(l_window);
		}
	}
	else if(playerVsplayer == 1)
	{
		if (!gameboard->insert(color, getScrollPosition())) return;
		else
		{
			this->getboard()->renderBoard(l_window);
			this->render(l_window);
			l_window.display();
			checkvictory(l_window);
		}
	}
	if (this->getboard()->isFull())
	{
		sf::Event event;

		WriteToScreen("Draw, Press R for New Game!", l_window);
		while ((l_window.pollEvent(event)))
		{
			this_thread::sleep_for(1000ms);
			l_window.display();
			if (event.key.code == sf::Keyboard::R)
				break;

		}
		this->getboard()->clearboard();
	}
	return;
}

board* game::getboard()
{
	return gameboard;
}


void game::updateScrollPositionUp()
{

	ScrollPosition == 6 ? ScrollPosition = 0 : ScrollPosition++;
	return;
}

void game::updateScrollPositionDown()
{
    ScrollPosition == 0 ? ScrollPosition = 6 : ScrollPosition--;
	return;
}

void game::render(sf::RenderWindow& l_window)
{
	sf::Texture arrowImage;
	arrowImage.loadFromFile("arrow.png");
	sf::Sprite arrow(arrowImage);
	arrow.setPosition(90*ScrollPosition, 440);
	l_window.draw(arrow);
}

const int game::getScrollPosition()
{
	return ScrollPosition;
}



void game::copyBoard(board*  inGame)
{
	for (int i = 0; i < BoardHeight; i++)
	{
		for (int j = 0; j < BoardWidth; j++)
		{
			gameboard->getChip(j,i)->setcolor(inGame->getChip(j,i)->getcolor());
		}
	}
}



void game::checkvictory(sf::RenderWindow & l_window)
{
	sf::Event event;
	if (this->getboard()->victory('R'))
	{
		WriteToScreen("Red Win, Press R for New Game!", l_window);
		while ((l_window.pollEvent(event)))
		{
			this_thread::sleep_for(1000ms);
			l_window.display();
			if (event.key.code == sf::Keyboard::R)
				break;

		}
		this->getboard()->clearboard();
	}
	if (this->getboard()->victory('B'))
	{

		WriteToScreen("Black Win, Press R for New Game!", l_window);

		while ((l_window.pollEvent(event)))
		{
			this_thread::sleep_for(1000ms);
			l_window.display();
			if (event.key.code == sf::Keyboard::R)
				break;
		}
		this->getboard()->clearboard();
	}

}
void game::setPvP(bool In)
{
	playerVsplayer = In;
	return;
}

const bool game::getPVP()
{
	return playerVsplayer;
}

void game::WriteToScreen(string inString, sf::RenderWindow & l_window)
{
	sf::Text text;
	sf::Font font;
	font.loadFromFile("arial.ttf");
	text.setFont(font);
	text.setOutlineColor(sf::Color::Black);
	text.setOutlineThickness(2);
	text.setString(inString);
	text.setCharacterSize(15);
	l_window.draw(text);
	return;
}

void game::setBoard(board * inBoard)
{
	gameboard = inBoard;
}

