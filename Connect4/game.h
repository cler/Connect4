#pragma once
#ifndef GAME_H
#define GAME_H
#include "Board.h"
#include <chrono>
#include <thread>
#include <SFML/Graphics.hpp>
class game
{
public:
	game();
	~game();
	void playGame(sf::RenderWindow& l_window, char color);
	board* getboard();
	void updateScrollPositionUp();
	void updateScrollPositionDown();
	void render(sf::RenderWindow& l_window);
	const int getScrollPosition();
	sf::Vector2i aiLose();
	sf::Vector2i aiWin();
	int aiEvaluate(); //returns value of selected column to go into
	void copyBoard(board* inGame);
	void checkvictory(sf::RenderWindow& l_window);
	void setPvP(bool In);
	const bool getPVP();
	void WriteToScreen(string inString, sf::RenderWindow& l_window);
	void setBoard(board* inBoard);
private:
	board* gameboard;
	bool playerVsplayer;
	int ScrollPosition;
};

#endif