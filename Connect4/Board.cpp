#include "Board.h"
#include <SFML/Graphics.hpp>

bool board::columnFull(int column)
{
	if (boardarray[column][0]->getcolor() == 'O') return 1;
	else return 0;
}
void board::printBoard()
{
	for (int i = 0; i < BoardHeight; i++)
	{
		for (int j = 0; j < BoardWidth; j++)
		{
			cout << boardarray[j][i]->getcolor();
		}
		cout << endl;
	}
	for (int j = 0; j < BoardWidth; j++)
	{
		cout << j;
	}
	cout << endl;
}
bool board::victory(char color)
{
	bool flag = 0;
	for (int i = 0; i < BoardHeight; i++)
	{
		for (int j = 0; j < BoardWidth; j++)
		{
			if (right(0, j, i, color) || left(0, j, i, color) || up(0, j, i, color) || down(0, j, i, color) || NE(0, j, i, color) || NW(0, j, i, color) || SE(0, j, i, color) || SW(0, j, i, color))
			{
				flag = 1;
				break;
			}
		}
		if (flag == 1) break;
	}
	return flag;
}
bool board::insert(char color, int column)
{
	chip* chipptr = boardarray[column][0];
	int i, helpptr = -2;
	for (i = -1; i < BoardHeight - 1; i++)
	{
		if (boardarray[column][i + 1]->getcolor() == 'O')
		{
			chipptr = boardarray[column][i + 1];
			helpptr = i + 1;
		}
	} // now we have the location to put the chip;
	if (helpptr == -2) return 0;
	chipptr->setcolor(color);
	chipptr->setposition(14+column*90, 6+(helpptr)*80);
	return 1;
}
board::board()
{
	for (int i = 0; i < BoardHeight; i++)
	{
		for (int j = 0; j < BoardWidth; j++)
		{
			boardarray[j][i] = new chip('O');
		}
	}
}
board::~board()
{
	for (int i = 0; i < BoardHeight; i++)
	{
		for (int j = 0; j < BoardWidth; j++)
		{
			delete boardarray[j][i];
		}
	}
}

void board::clearboard()
{
	for (int i = 0; i < BoardHeight; i++)
	{
		for (int j = 0; j < BoardWidth; j++)
		{
			boardarray[j][i]->setcolor('O');
		}
	}
}
chip * board::getChip(int j, int i)
{
	return boardarray[j][i];
}

bool board::isFull()
{
	bool flag = 1;

	for (int j = 0; j < BoardWidth; j++)
	{
		if (boardarray[j][0]->getcolor() == 'O')
		{
			flag = 0;
			break;
		}
	}

	
	return flag;
}

bool board::isColumnFull(int i)
{
	if (boardarray[i][0]->getcolor() == 'O') return false;
	else return true;
}

chip* board::getboard()
{
	return boardarray[0][0];
}

int board::getTop(int column)
{
	int Top = 0; 
	for (int i = BoardHeight - 1; i >= 1; i--)
	{
		if (boardarray[column][i]->getcolor() != 'O')
		{
			Top = i;
			break;
		}
	}
	return Top;
}

int board::check3(int j, int i, char color)
{
	int counter = 0;
	if (right(2, j+1, i, color)) counter++;
	if (left(2, j-1, i, color)) counter++;
	if (down(2, j, i-1, color)) counter++;
	if (up(2, j, i+1, color)) counter++;
	if (NE(2, j+1, i+1, color)) counter++;
	if (NW(2, j-1, i+1, color)) counter++;
	if (SE(2, j+1, i-1, color)) counter++;
	if (SW(2, j-1, i-1, color)) counter++;
	return counter;
}

bool board::right(int count, int column, int row, char color)
{
	if (count == 4) return 1;
	if (column >= BoardWidth || row >= BoardHeight || column < 0 || row < 0) return 0;
	if (boardarray[column][row]->getcolor() != color) return 0;
	return right(count + 1, column + 1, row, color);
}

bool board::left(int count, int column, int row, char color)
{
	if (count == 4) return 1;
	if (column >= BoardWidth || row >= BoardHeight || column < 0 || row < 0) return 0;
	if (boardarray[column][row]->getcolor() != color) return 0;
	return left(count + 1, column - 1, row, color);
}

bool board::up(int count, int column, int row, char color)
{
	if (count == 4) return 1;
	if (column >= BoardWidth || row >= BoardHeight || column < 0 || row < 0) return 0;
	if (boardarray[column][row]->getcolor() != color) return 0;
	return up(count + 1, column, row + 1, color);
}

bool board::down(int count, int column, int row, char color)
{
	if (count == 4) return 1;
	if (column >= BoardWidth || row >= BoardHeight || column < 0 || row < 0) return 0;
	if (boardarray[column][row]->getcolor() != color) return 0;
	return down(count + 1, column, row - 1, color);
}
bool board::NE(int count, int column, int row, char color)
{
	if (count == 4) return 1;
	if (column >= BoardWidth || row >= BoardHeight || column < 0 || row < 0) return 0;
	if (boardarray[column][row]->getcolor() != color) return 0;
	return NE(count + 1, column + 1, row + 1, color);
}

bool board::NW(int count, int column, int row, char color)
{
	if (count == 4) return 1;
	if (column >= BoardWidth || row >= BoardHeight || column < 0 || row < 0) return 0;
	if (boardarray[column][row]->getcolor() != color) return 0;
	return NW(count + 1, column + 1, row - 1, color);
}

bool board::SE(int count, int column, int row, char color)
{
	if (count == 4) return 1;
	if (column >= BoardWidth || row >= BoardHeight || column < 0 || row < 0) return 0;
	if (boardarray[column][row]->getcolor() != color) return 0;
	return SE(count + 1, column - 1, row + 1, color);
}

bool board::SW(int count, int column, int row, char color)
{
	if (count == 4) return 1;
	if (column >= BoardWidth || row >= BoardHeight || column < 0 || row < 0) return 0;
	if (boardarray[column][row]->getcolor() != color) return 0;
	return SW(count + 1, column - 1, row - 1, color);
}


void board::renderBoard(sf::RenderWindow& l_window)
{
	sf::CircleShape chipIn;
	for (int i = 0; i < BoardHeight; i++)
	{
		for (int j = 0; j < BoardWidth; j++)
		{
			if (boardarray[j][i]->getcolor() != 'O')
			{
			if (boardarray[j][i]->getcolor() == 'R') chipIn.setFillColor(sf::Color::Red);
			else if ((boardarray[j][i]->getcolor() == 'B')) chipIn.setFillColor(sf::Color::Black);
			else chipIn.setFillColor(sf::Color::Green);
			chipIn.setPosition(boardarray[j][i]->getposition());
			chipIn.setRadius(36);
			l_window.draw(chipIn);
			}
		}
	}
}