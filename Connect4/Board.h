#ifndef BOARD_H
#define BOARD_H
#include <iostream>
#include "Chip.h"

using namespace std;
const int BoardWidth = 7;
const int BoardHeight = 6;

class board //7 holes horizonal, 6 holes vertical.
{
private:
	chip* boardarray[BoardWidth][BoardHeight];
	bool right(int count, int column, int row, char color);
	bool left(int count, int column, int row, char color);
	bool up(int count, int column, int row, char color);
	bool down(int count, int column, int row, char color);
	bool NE(int count, int column, int row, char color);
	bool NW(int count, int column, int row, char color);
	bool SE(int count, int column, int row, char color);
	bool SW(int count, int column, int row, char color);

public:
	void renderBoard(sf::RenderWindow& l_window);
	bool columnFull(int column);
	void printBoard();
	bool victory(char color);
	bool insert(char color, int column);
	board();
	~board();
	void clearboard();
	chip* getChip(int j, int i);
	bool isFull();
	bool isColumnFull(int i);
	chip* getboard(); 
	int getTop(int column);
	int check3(int j, int i, char color); //Checks if placing a chip in a spot will cause a 3 in a row.
};



#endif