#pragma once

#include "game.h"
#include "Board.h"
typedef pair<int, int> Pair;

class Node
{
public:
	Node(char inColor);
	Node();
	const char getColor();
	const int getScore();
	game* getGame();
	Node* getChild(int iterator);
	void setColor(char inColor);
	void setScore(int inScore);
	void setGame(game* inGame);
	void setChild(Node* inNode, int Index);
	void setParent(Node* inNode);
private:
	char color;
	int score;
	Node* Children[BoardWidth];//Should consider using Node**
	Node* Parent;
	game* PsuedoGame;
};
class Tree
{
public:
	Tree(game* inGame);
	~Tree();
	Pair findBestValue();//If is leaf, will find value. If isn't leaf then find min/max of leaf values.
	void print();//For debugging purposes, I may implement this later.
	Node* GetRootNode();

	int findVal(int inVal);
private:
	Node * RootNode;
	void BuildTree(int height, int maxHeight, Node* parent, game* Game);
	int EvaluateTree(Node * Root);//Consider moving this and buildTree to the constructor
	int MaxGeneration;


};

